# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import pytest
import test_lvcnrcheck_helper as helper
import h5py as h5
import re
import numpy as np

templateOutput = """# Format 1

## General Fields

- [=] type (NRinjection)
- [=] Format (1)
- [=] name (1.0)
- [=] alternative-names (1.0)
- [=] NR-group (1.0)
- [=] NR-code (1.0)
- [=] modification-date (1.0)
- [=] point-of-contact-email (1.0)
- [=] INSPIRE-bibtex-keys (1.0)
- [=] license (LVC-internal)
- [=] Lmax (1)
- [=] simulation-type (aligned-spins)
- [=] auxiliary-info (<class 'h5py._hl.group.Group'>)
- [=] NR-techniques (1.0)

## Error Assessment

- [=] files-in-error-series (1.0)
- [=] comparable-simulation (1.0)
- [=] production-run (0)

## CBC Parameters

- [=] object1 (BH)
- [=] object2 (BH)
- [=] mass1 (4.0)
- [=] mass2 (1.0)
- [=] eta (0.16)
- [=] f_lower_at_1MSUN (1.0)
- [=] spin1x (1.0)
- [=] spin1y (1.0)
- [=] spin1z (1.0)
- [=] spin2x (1.0)
- [=] spin2y (1.0)
- [=] spin2z (1.0)
- [=] LNhatx (1.0)
- [=] LNhaty (1.0)
- [=] LNhatz (1.0)
- [=] nhatx (1.0)
- [=] nhaty (1.0)
- [=] nhatz (1.0)
- [=] Omega (1.0)
- [=] eccentricity (1.0)
- [=] mean_anomaly (1.0)

# Format 1 (Interfield)

- [=] mass-ordering (mass1 >= mass2)
- [=] peak-near-zero (waveform peak is at 0.05M which is less than 10.00M from zero)
- [=] phase-sense ((2,2) phase is decreasing on average)

# Format 2

- [=] mass1-vs-time (<class 'h5py._hl.group.Group'>)
- [=] mass2-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin1x-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin1y-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin1z-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin2x-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin2y-vs-time (<class 'h5py._hl.group.Group'>)
- [=] spin2z-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position1x-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position1y-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position1z-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position2x-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position2y-vs-time (<class 'h5py._hl.group.Group'>)
- [=] position2z-vs-time (<class 'h5py._hl.group.Group'>)
- [=] LNhatx-vs-time (<class 'h5py._hl.group.Group'>)
- [=] LNhaty-vs-time (<class 'h5py._hl.group.Group'>)
- [=] LNhatz-vs-time (<class 'h5py._hl.group.Group'>)
- [=] Omega-vs-time (<class 'h5py._hl.group.Group'>)

# Format 2 (Interfield)

- [=] mass-vs-time-ordering (mass1-vs-time >= mass2-vs-time)

# Format 3

- [=] remnant-mass-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-spinx-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-spiny-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-spinz-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-positionx-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-positiony-vs-time (<class 'h5py._hl.group.Group'>)
- [=] remnant-positionz-vs-time (<class 'h5py._hl.group.Group'>)

# Phase Modes

- [=] phase_l2_m-1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l2_m-2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l2_m0 (<class 'h5py._hl.group.Group'>)
- [=] phase_l2_m1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l2_m2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m-1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m-2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m-3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m0 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l3_m3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m-1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m-2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m-3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m-4 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m0 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l4_m4 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m-1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m-2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m-3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m-4 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m-5 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m0 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m1 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m2 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m3 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m4 (<class 'h5py._hl.group.Group'>)
- [=] phase_l5_m5 (<class 'h5py._hl.group.Group'>)

# Amplitude Modes

- [=] amp_l2_m-1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l2_m-2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l2_m0 (<class 'h5py._hl.group.Group'>)
- [=] amp_l2_m1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l2_m2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m-1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m-2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m-3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m0 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l3_m3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m-1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m-2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m-3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m-4 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m0 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l4_m4 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m-1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m-2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m-3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m-4 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m-5 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m0 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m1 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m2 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m3 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m4 (<class 'h5py._hl.group.Group'>)
- [=] amp_l5_m5 (<class 'h5py._hl.group.Group'>)"""


class TestValidFile(object):

    def setup(self):
        self.output = templateOutput
        self.f = helper.createValidSim()

    def test_valid_file(self):
        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 0


class TestField(object):
    """Base for testing individual fields

    Attributes
    ----------
    name: str
        Name of field to test.
    f: NamedTemporaryFile
        Temporary LVC NR Waveform input file.
    output: str
        Expected output from `lvcnrcheck` with `f` as input.
    """

    name = 'field'

    def setup(self):
        self.f = helper.createValidSim()
        self.output = templateOutput

    def setValue(self, value):
        """Set the value of the represented field"""
        self.setNamedField(self.name, value)

    def setOutput(self, output):
        """Set the output line of `lvcnrcheck` for the represented field"""
        self.setNamedOutput(self.name, output)

    def setNamedField(self, name, value):
        """Set the value of the named field"""
        nr = h5.File(self.f.name)
        nr.attrs[name] = value
        nr.close()

    def setNamedDataset(self, name, value):
        """Set the value of the named dataset"""
        nr = h5.File(self.f.name)
        nr[name][:] = value
        nr.close()

    def setNamedOutput(self, name, output):
        """Set the output line of `lvcnrcheck` for the named field"""
        self.output = re.sub(
            r'^- \[=\] {0:s} .*?$'.format(name),
            output, self.output, flags=re.MULTILINE)


class TestGroup(TestField):
    """Base for testing individual group fields"""

    def convertToDataset(self):
        """Convert the group to a dataset of the represented field"""
        nr = h5.File(self.f.name)
        del nr[self.name]
        data = np.array([i for i in range(10)])
        nr.create_dataset(self.name, data=data)
        nr.close()


class TestROMSpline(TestField):
    """Base for testing individual ROMSpline group fields"""

    def convertToDataset(self):
        """Convert the group to a dataset of the represented field"""
        nr = h5.File(self.f.name)
        del nr[self.name]
        data = np.array([i for i in range(10)])
        nr.create_dataset(self.name, data=data)
        nr.close()

    def setSubfields(self, subfields):
        """Set the subfields of the ROMSpline group field"""
        nr = h5.File(self.f.name)
        del nr[self.name]
        group = nr.create_group(self.name)
        for sub in subfields:
            data = np.array([float(i) for i in range(10)])
            group.create_dataset(sub, data=data)
        nr.close()


class TestInterfield(TestField):
    """Base for testing field relationships"""
    name = 'interfield'


class TestType(TestField):

    name = 'type'

    def test_invalid_type(self):
        # Invalidate fields type
        self.setValue(1.0)

        # Update template output for fields invalid type
        self.setOutput(
            '- [WRONG TYPE] type (1.0) (Type must be <type \'basestring\'>)')

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        # Invalidate fields value
        self.setValue('invalid')

        # Update template output for fields invalid value
        self.setOutput(
            ('- [INVALID VALUE] type (invalid) '
             '(Value must be one of NRinjection)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestFormat(TestField):

    name = 'Format'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            '- [WRONG TYPE] Format (1.0) (Type must be <type \'int\'>)')

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue(0)
        self.setOutput(
            ('- [INVALID VALUE] Format (0) '
             '(Value must be one of 1, 2, 3)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSimulationType(TestField):

    name = 'simulation-type'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] simulation-type (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [INVALID VALUE] simulation-type (invalid) '
             '(Value must be one of aligned-spins, non-spinning, precessing)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestName(TestField):

    name = 'name'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] name (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestAlternativeNames(TestField):

    name = 'alternative-names'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] alternative-names (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNRGroup(TestField):

    name = 'NR-group'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] NR-group (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNRCode(TestField):

    name = 'NR-code'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] NR-code (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestModificationDate(TestField):

    name = 'modification-date'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] modification-date (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPointOfContactEmail(TestField):

    name = 'point-of-contact-email'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] point-of-contact-email (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestINSPIREBibtexKeys(TestField):

    name = 'INSPIRE-bibtex-keys'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] INSPIRE-bibtex-keys (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLicense(TestField):

    name = 'license'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] license (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [INVALID VALUE] license (invalid) '
             '(Value must be one of LVC-internal, public)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLmax(TestField):

    name = 'Lmax'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            '- [WRONG TYPE] Lmax (1.0) (Type must be <type \'int\'>)')

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestAuxiliaryInfo(TestGroup):

    name = 'auxiliary-info'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] auxiliary-info '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNRTechniques(TestField):

    name = 'NR-techniques'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] NR-techniques (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestFilesInErrorSeries(TestField):

    name = 'files-in-error-series'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] files-in-error-series (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestComparableSimulation(TestField):

    name = 'comparable-simulation'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] comparable-simulation (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestProductionRun(TestField):

    name = 'production-run'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] production-run (1.0) '
             '(Type must be <type \'int\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue(2)
        self.setOutput(
            ('- [INVALID VALUE] production-run (2) '
             '(Value must be one of 0, 1)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestObject1(TestField):

    name = 'object1'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] object1 (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [INVALID VALUE] object1 (invalid) '
             '(Value must be one of BH, NS)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestObject2(TestField):

    name = 'object2'

    def test_invalid_type(self):
        self.setValue(1.0)
        self.setOutput(
            ('- [WRONG TYPE] object2 (1.0) '
             '(Type must be <type \'basestring\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_value(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [INVALID VALUE] object2 (invalid) '
             '(Value must be one of BH, NS)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMass1(TestField):

    name = 'mass1'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] mass1 (invalid) '
             '(Type must be <type \'float\'>)'))
        self.setNamedOutput(
            'mass-ordering',
            ('- [INVALID FIELDS] mass-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMass2(TestField):

    name = 'mass2'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] mass2 (invalid) '
             '(Type must be <type \'float\'>)'))
        self.setNamedOutput(
            'mass-ordering',
            ('- [INVALID FIELDS] mass-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestEta(TestField):

    name = 'eta'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] eta (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestFLowerAt1MSUN(TestField):

    name = 'f_lower_at_1MSUN'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] f_lower_at_1MSUN (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1x(TestField):

    name = 'spin1x'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin1x (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1y(TestField):

    name = 'spin1y'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin1y (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1z(TestField):

    name = 'spin1z'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin1z (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2x(TestField):

    name = 'spin2x'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin2x (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2y(TestField):

    name = 'spin2y'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin2y (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2z(TestField):

    name = 'spin2z'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] spin2z (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhatx(TestField):

    name = 'LNhatx'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] LNhatx (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhaty(TestField):

    name = 'LNhaty'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] LNhaty (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhatz(TestField):

    name = 'LNhatz'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] LNhatz (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNhatx(TestField):

    name = 'nhatx'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] nhatx (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNhaty(TestField):

    name = 'nhaty'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] nhaty (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestNhatz(TestField):

    name = 'nhatz'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] nhatz (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestOmega(TestField):

    name = 'Omega'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] Omega (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestEccentricity(TestField):

    name = 'eccentricity'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] eccentricity (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMeanAnomaly(TestField):

    name = 'mean_anomaly'

    def test_invalid_type(self):
        self.setValue('invalid')
        self.setOutput(
            ('- [WRONG TYPE] mean_anomaly (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMassOrdering(TestInterfield):

    name = 'mass-ordering'

    def test_invalid_mass1(self):
        self.setOutput(
            ('- [INVALID FIELDS] mass-ordering '
             '(Field dependencies are invalid)'))
        self.setNamedField('mass1', 'invalid')
        self.setNamedOutput(
            'mass1',
            ('- [WRONG TYPE] mass1 (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_mass2(self):
        self.setOutput(
            ('- [INVALID FIELDS] mass-ordering '
             '(Field dependencies are invalid)'))
        self.setNamedField('mass2', 'invalid')
        self.setNamedOutput(
            'mass2',
            ('- [WRONG TYPE] mass2 (invalid) '
             '(Type must be <type \'float\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_ordering(self):
        self.setOutput(
            ('- [INVALID INTERFIELD] mass-ordering '
             '(mass1 < mass2)'))
        self.setNamedField('mass1', 1.0)
        self.setNamedOutput('mass1', '- [=] mass1 (1.0)')
        self.setNamedField('mass2', 4.0)
        self.setNamedOutput('mass2', '- [=] mass2 (4.0)')

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMass1VsTime(TestROMSpline):

    name = 'mass1-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] mass1-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))
        self.setNamedOutput(
            'mass-vs-time-ordering',
            ('- [INVALID FIELDS] mass-vs-time-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] mass1-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))
        self.setNamedOutput(
            'mass-vs-time-ordering',
            ('- [INVALID FIELDS] mass-vs-time-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestMass2VsTime(TestROMSpline):

    name = 'mass2-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] mass2-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))
        self.setNamedOutput(
            'mass-vs-time-ordering',
            ('- [INVALID FIELDS] mass-vs-time-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] mass2-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))
        self.setNamedOutput(
            'mass-vs-time-ordering',
            ('- [INVALID FIELDS] mass-vs-time-ordering '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1xVsTime(TestROMSpline):

    name = 'spin1x-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin1x-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin1x-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1yVsTime(TestROMSpline):

    name = 'spin1y-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin1y-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin1y-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin1zVsTime(TestROMSpline):

    name = 'spin1z-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin1z-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin1z-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2xVsTime(TestROMSpline):

    name = 'spin2x-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin2x-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin2x-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2yVsTime(TestROMSpline):

    name = 'spin2y-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin2y-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin2y-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestSpin2zVsTime(TestROMSpline):

    name = 'spin2z-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] spin2z-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] spin2z-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition1xVsTime(TestROMSpline):

    name = 'position1x-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position1x-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position1x-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition1yVsTime(TestROMSpline):

    name = 'position1y-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position1y-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position1y-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition1zVsTime(TestROMSpline):

    name = 'position1z-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position1z-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position1z-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition2xVsTime(TestROMSpline):

    name = 'position2x-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position2x-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position2x-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition2yVsTime(TestROMSpline):

    name = 'position2y-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position2y-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position2y-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPosition2zVsTime(TestROMSpline):

    name = 'position2z-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] position2z-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] position2z-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhatxVsTime(TestROMSpline):

    name = 'LNhatx-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] LNhatx-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] LNhatx-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhatyVsTime(TestROMSpline):

    name = 'LNhaty-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] LNhaty-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] LNhaty-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestLNhatzVsTime(TestROMSpline):

    name = 'LNhatz-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] LNhatz-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] LNhatz-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestOmegaVsTime(TestROMSpline):

    name = 'Omega-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] Omega-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] Omega-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_monotonicity(self):
        self.setOutput(
            ('- [INVALID SEQUENCE] Omega-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Omega is not monotonic: Suggest downgrading to Format=1)'))
        self.setNamedDataset('Omega-vs-time/Y', np.array([0,1,2,4,3,5,6,7,8,9]))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

class TestMassVsTimeOrdering(TestInterfield):

    name = 'mass-vs-time-ordering'

    def test_invalid_ordering(self):
        self.setOutput(
            ('- [INVALID INTERFIELD] mass-vs-time-ordering '
             '(mass1-vs-time < mass2-vs-time)'))
        self.setNamedDataset('mass1-vs-time/Y', np.array([1.0]*10))
        self.setNamedDataset('mass2-vs-time/Y', np.array([4.0]*10))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantMassVsTime(TestROMSpline):

    name = 'remnant-mass-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-mass-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-mass-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantSpinxVsTime(TestROMSpline):

    name = 'remnant-spinx-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-spinx-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-spinx-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantSpinyVsTime(TestROMSpline):

    name = 'remnant-spiny-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-spiny-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-spiny-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantSpinzVsTime(TestROMSpline):

    name = 'remnant-spinz-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-spinz-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-spinz-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantPositionxVsTime(TestROMSpline):

    name = 'remnant-positionx-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-positionx-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-positionx-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantPositionyVsTime(TestROMSpline):

    name = 'remnant-positiony-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-positiony-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-positiony-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestRemnantPositionzVsTime(TestROMSpline):

    name = 'remnant-positionz-vs-time'

    def test_invalid_type(self):
        self.convertToDataset()
        self.setOutput(
            ('- [WRONG TYPE] remnant-positionz-vs-time '
             '(<class \'h5py._hl.dataset.Dataset\'>) '
             '(Type must be <class \'h5py._hl.group.Group\'>)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ('- [INVALID SUBFIELDS] remnant-positionz-vs-time '
             '(<class \'h5py._hl.group.Group\'>) '
             '(Field has subfields [invalid-1, invalid-2] '
             'but should have [X, Y, deg, errors, tol])'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPhaselm(TestROMSpline):

    @pytest.fixture(
        params=[
            'phase_l2_m-1',
            'phase_l2_m-2',
            'phase_l2_m0',
            'phase_l2_m1',
            'phase_l2_m2',
            'phase_l3_m-1',
            'phase_l3_m-2',
            'phase_l3_m-3',
            'phase_l3_m0',
            'phase_l3_m1',
            'phase_l3_m2',
            'phase_l3_m3',
            'phase_l4_m-1',
            'phase_l4_m-2',
            'phase_l4_m-3',
            'phase_l4_m-4',
            'phase_l4_m0',
            'phase_l4_m1',
            'phase_l4_m2',
            'phase_l4_m3',
            'phase_l4_m4',
            'phase_l5_m-1',
            'phase_l5_m-2',
            'phase_l5_m-3',
            'phase_l5_m-4',
            'phase_l5_m-5',
            'phase_l5_m0',
            'phase_l5_m1',
            'phase_l5_m2',
            'phase_l5_m3',
            'phase_l5_m4',
            'phase_l5_m5'])
    def name(self, request):
        self.name = request.param
        return request.param

    def test_invalid_type(self, name):
        self.convertToDataset()
        self.setOutput(
            ("- [WRONG TYPE] {0:s} "
             "(<class \'h5py._hl.dataset.Dataset\'>) "
             "(Type must be <class \'h5py._hl.group.Group\'>)").format(name))
        if name == 'phase_l2_m2':
            self.setNamedOutput(
                'phase-sense',
                ('- [INVALID FIELDS] phase-sense '
                 '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self, name):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ("- [INVALID SUBFIELDS] {0:s} "
             "(<class \'h5py._hl.group.Group\'>) "
             "(Field has subfields [invalid-1, invalid-2] "
             "but should have [X, Y, deg, errors, tol])").format(name))
        if name == 'phase_l2_m2':
            self.setNamedOutput(
                'phase-sense',
                ('- [INVALID FIELDS] phase-sense '
                 '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestAmplm(TestROMSpline):

    @pytest.fixture(
        params=[
            'amp_l2_m-1',
            'amp_l2_m-2',
            'amp_l2_m0',
            'amp_l2_m1',
            'amp_l2_m2',
            'amp_l3_m-1',
            'amp_l3_m-2',
            'amp_l3_m-3',
            'amp_l3_m0',
            'amp_l3_m1',
            'amp_l3_m2',
            'amp_l3_m3',
            'amp_l4_m-1',
            'amp_l4_m-2',
            'amp_l4_m-3',
            'amp_l4_m-4',
            'amp_l4_m0',
            'amp_l4_m1',
            'amp_l4_m2',
            'amp_l4_m3',
            'amp_l4_m4',
            'amp_l5_m-1',
            'amp_l5_m-2',
            'amp_l5_m-3',
            'amp_l5_m-4',
            'amp_l5_m-5',
            'amp_l5_m0',
            'amp_l5_m1',
            'amp_l5_m2',
            'amp_l5_m3',
            'amp_l5_m4',
            'amp_l5_m5'])
    def name(self, request):
        self.name = request.param
        return request.param

    def test_invalid_type(self, name):
        self.convertToDataset()
        self.setOutput(
            ("- [WRONG TYPE] {0:s} "
             "(<class \'h5py._hl.dataset.Dataset\'>) "
             "(Type must be <class \'h5py._hl.group.Group\'>)").format(name))
        self.setNamedOutput(
            'peak-near-zero',
            ('- [INVALID FIELDS] peak-near-zero '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_subfields(self, name):
        self.setSubfields(['invalid-1', 'invalid-2'])
        self.setOutput(
            ("- [INVALID SUBFIELDS] {0:s} "
             "(<class \'h5py._hl.group.Group\'>) "
             "(Field has subfields [invalid-1, invalid-2] "
             "but should have [X, Y, deg, errors, tol])").format(name))
        self.setNamedOutput(
            'peak-near-zero',
            ('- [INVALID FIELDS] peak-near-zero '
             '(Field dependencies are invalid)'))

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPeakNearZero(TestInterfield):

    name = 'peak-near-zero'

    def test_invalid_peak_location(self):
        self.setOutput(
            ('- [INVALID INTERFIELD] peak-near-zero (waveform peak is at '
             '-99.95M which is greater than 10.00M from zero)'))

        nr = h5.File(self.f.name)
        keys = [k for k in nr.keys() if k.startswith('amp_l')]
        for k in keys:
            nr[k]['X'][:] -= 100.0
        nr.close()

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1


class TestPhaseSense(TestInterfield):

    name = 'phase-sense'

    def test_invalid_phase_sense(self):
        self.setOutput(
            ('- [INVALID INTERFIELD] phase-sense '
             '((2,2) phase is not decreasing on average)'))

        nr = h5.File(self.f.name)
        nr['phase_l2_m2']['Y'][:] *= -1.0
        nr.close()

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_phase_zero_inspiral(self):
        self.setOutput(
            ('- [INVALID FIELDS] phase-sense '
             '(Field dependencies are invalid)'))

        nr = h5.File(self.f.name)
        nr['phase_l2_m2']['X'][:] -= nr['phase_l2_m2']['X'][0]
        nr.close()

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1

    def test_invalid_phase_positive_inspiral(self):
        self.setOutput(
            ('- [INVALID FIELDS] phase-sense '
             '(Field dependencies are invalid)'))

        nr = h5.File(self.f.name)
        nr['phase_l2_m2']['X'][:] += 10.0 - nr['phase_l2_m2']['X'][0]
        nr.close()

        (output, returncode) = helper.lvcnrcheck(['-f', '3', self.f.name], returncode=True)
        assert output.strip() == self.output
        assert returncode == 1
